<!-- cargo-sync-readme start -->

# Semihosting or ITM logger for cortex-m applications

<!-- cargo-sync-readme end -->

[![version](https://img.shields.io/crates/v/cortex-m-logger.svg)](https://crates.io/crates/cortex-m-logger/)
[![documentation](https://docs.rs/cortex-m-logger/badge.svg)](https://docs.rs/cortex-m-logger/)
[![license](https://img.shields.io/crates/l/cortex-m-logger.svg)](https://crates.io/crates/cortex-m-logger/)
[![pipeline status](https://gitlab.com/jrobsonchase/cortex-m-logger/badges/master/pipeline.svg)](https://gitlab.com/jrobsonchase/cortex-m-logger/pipelines/)

## License

Licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or <http://www.apache.org/licenses/LICENSE-2.0>)
* MIT license ([LICENSE-MIT](LICENSE-MIT) or <http://opensource.org/licenses/MIT>)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.
